import numpy as np

def hx(i,j,n,m,k):
    return -np.abs(float(i)/float(m-k)-float(j)/float(n))
def hy(i,j,n,m,k):
    return -np.abs(float(i-k)/float(m-k)-float(j)/float(n))

# i = position in signal
# j = word position
# n = sentence length
# m = signal length
# lamda = 0.5
# mu = arbitrary?
def dx(i,j,n,m,mu,lamda=0.5):
    d = np.empty([n+2*mu])
    tempsum = 0.0
    for it in range(-mu,n+mu):
        if n > 1:
            d[it+mu] = np.exp(lamda*hx(i+1,it,n-1,m,mu))
        else:
            d[it+mu] = np.exp(lamda*hx(i+1,it,1,m,mu))
        tempsum += d[it+mu]
    return d[j+mu]/tempsum

def dy(i,j,n,m,mu,lamda=0.5):
    d = np.empty([n+2*mu])
    tempsum = 0.0
    for it in range(-mu,n+mu):
        if n > 1:
            d[it+mu] = np.exp(lamda*hy(i+1,it,n-1,m,mu))
        else:
            d[it+mu] = np.exp(lamda*hy(i+1,it,1,m,mu))
        tempsum += d[it+mu]
    return d[j+mu]/tempsum

