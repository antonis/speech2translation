#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import codecs
import string
from collections import defaultdict
import fastd
import os 
import resource
import cPickle as pickle
from scipy.spatial import distance
import bisect
import itertools
from multiprocessing import Pool


# Functions used for making deafaultdicts pickleable
def dd():
    return 0
def dd2():
    return -10
def dd3():
    return []
def dd4():
    return (0,0)

# Global variables
aligns = []
vocab = [[]]
Prototype = []
prob = defaultdict(dd2)
mapkey = defaultdict(dd4)
count = defaultdict(dd)
DX = defaultdict(dd)
DY = defaultdict(dd)
searchSpace = []
MFCCptr = []

# One iteration of DBA
def DBA_one_iter(average,s):
    global MFCCptr
    tAss = []
    for t in xrange(len(average)):
        tAss.append([])
    for seq in s:
        _,tr = dtw(MFCCptr[seq[0]][seq[1]:seq[2]],average)
        for j,i in enumerate(tr[1]):
            tAss[i].append(MFCCptr[seq[0]][seq[1]+tr[0][j]])
    for t in xrange(len(average)):
        if tAss[t]:
            average[t] = np.mean(np.array(tAss[t]), axis=0)
        else:
            average[t] = np.zeros([39])
    return

# DBA
def dba(s, itern=4, verbose=False):
	    # s is the set of sequences to be averaged
        # itern is the number of iterations to run
        global vocab
        global MFCCptr
        if len(vocab[s]) == 0:
            return []
        elif len(vocab[s]) == 1:
            return np.array(MFCCptr[vocab[s][0][0]][vocab[s][0][1]:vocab[s][0][2]])
        
        # New: Select average to also have median length
        lens = [x[2]-x[1] for x in vocab[s]]
        med = np.floor(np.median(np.array(lens)))
        #Make sure median length is in list
        while med not in lens:
            med += 1
        index = lens.index(med)
        average = np.array(MFCCptr[vocab[s][index][0]][vocab[s][index][1]:vocab[s][index][2]])
	
        if (verbose):
            print "Picked the ", index, "-th seq to start"
	    for i in xrange(itern):
		        if (verbose):
			        print "Iteration ", i, " average: ", average
		        DBA_one_iter(average, vocab[s])
        return average 

def dtw(x,y,doNorm=True):
    """ Computes the DTW of two sequences. 
    :param array x: N1*M array
    :param array y: N2*M array
    Returns the minimum distance and the warp path.
    """
    if len(x.shape) == 1:
        x = x.reshape(-1,1)
    if len(y.shape) == 1:
        y= y.reshape(-1,1)
    r, c = len(x), len(y)
    D = np.zeros((r+1, c+1))
    D[0, 1:] = np.inf
    D[1:, 0] = np.inf
    D[1:,1:] = distance.cdist(x,y,'euclidean')
    for i in range(r):
        for j in range(c):
            D[i+1, j+1] += min(D[i,j], D[i, j+1], D[i+1,j])
    if doNorm:
        dist = D[-1,-1]/(float(r+c))
    else:
        dist = D[-1,-1]
    
    return dist, _traceback(D)

def dtw_window(x,y,N=30,doNorm=True):
    """ Computes the DTW of two sequences respecting a window of N frames. 
    :param array x: N1*M array
    :param array y: N2*M array
    Returns the minimum distance and the warp path.
    """
    if len(x.shape) == 1:
        x = x.reshape(-1,1)
    if len(y.shape) == 1:
        y= y.reshape(-1,1)
    r, c = len(x), len(y)
    D = np.zeros((r+1, c+1)) + np.inf
    D[0, 1:] = np.inf
    D[1:, 0] = np.inf
    D[0, 0] = 0
    window = max(N,abs(r-c))
    #D[1:,1:] = distance.cdist(x,y,'euclidean')
    for i in range(r):
        for j in range(max(1,i-window),min(c,i+window)):
            D[i+1, j+1] = np.linalg.norm(x[i]-y[j])
            D[i+1, j+1] += min(D[i,j], D[i, j+1], D[i+1,j])
    if doNorm:
        dist = D[-1,-1]/(float(r+c))
    else:
        dist = D[-1,-1]
    
    return dist, _traceback(D)

# Finds the warp path
def _traceback(D):
    i, j = array(D.shape) - 1
    p,q = [i], [j]
    while (i>0 and j>0):
        tb = argmin((D[i-1,j-1], D[i-1,j], D[i,j-1]))
        if tb == 0:
            i -= 1
            j -= 1
        elif tb == 1:
            i -=1
        elif tb == 2:
            j -= 1

        p.insert(0, i)
        q.insert(0, j)
    p.insert(0,0)
    q.insert(0,0)
    return (array(p),array(q))

# Computes the DTW distance using a wrap window
def score_window(x,y,N=30,doNorm=True):
    """ Computes the DTW score of two sequences. 
    :param array x: N1*M array
    :param array y: N2*M array
    :param bool doNorm: decides whether to normalize the score
    Returns the minimum distance.
    """
    if len(x.shape) == 1:
        x = x.reshape(-1,1)
    if len(y.shape) == 1:
        y= y.reshape(-1,1)
    r, c = len(x), len(y)
    if abs(r-c)>50:
        return 0.999
    D = np.zeros((r+1, c+1)) + np.inf
    D[0, 1:] = np.inf
    D[1:, 0] = np.inf
    D[0, 0] = 0
    window = max(N,abs(r-c))
    #D[1:,1:] = distance.cdist(x,y,'euclidean')
    for i in range(r):
        for j in range(max(1,i-window),min(c,i+window)):
            D[i+1, j+1] = np.linalg.norm(x[i]-y[j])
            D[i+1, j+1] += min(D[i,j], D[i, j+1], D[i+1,j])
    if doNorm:
        dist = D[-1,-1]/(float(len(x[0])*(r+c)))
        #Should not be used ever
        if dist >= 1:
            dist = 0.999
    else:
        dist = D[-1,-1]
    
    return dist

# Computes the DTW distance
def score(x,y,doNorm=True):
    """ Computes the DTW score of two sequences. 
    :param array x: N1*M array
    :param array y: N2*M array
    :param bool doNorm: decides whether to normalize the score
    Returns the minimum distance.
    """
    if len(x.shape) == 1:
        x = x.reshape(-1,1)
    if len(y.shape) == 1:
        y= y.reshape(-1,1)
    r, c = len(x), len(y)
    if abs(r-c)>30:
        return 0.999
    D = np.zeros((r+1, c+1))
    D[0, 1:] = np.inf
    D[1:, 0] = np.inf
    D[1:,1:] = distance.cdist(x,y,'euclidean')
    for i in range(r):
        for j in range(c):
            D[i+1, j+1] += min(D[i,j], D[i, j+1], D[i+1,j])
    if doNorm:
        dist = D[-1,-1]/(float(len(x[0])*(r+c)))
        # Shouldn't be used but making sure
        if dist >= 1:
            dist = 0.999
    else:
        dist = D[-1,-1]
    
    return dist

# Check if candidate boudnaries include a silence
def checkSpace(s,cand1,cand2):
    left = int(cand1)
    right = int(cand2)
    for ss in s:
        #bound 1 in silence
        if cand1 >= ss[0] and cand1 < ss[1]:
            left = ss[1]
        #bound 2 in silence
        if cand2 >= ss[0] and cand2 <= ss[1]:
            right = ss[0]
        if cand1 >= ss[0] and cand1 <=ss[1] and cand2 >= ss[0] and cand2 <= ss[1]:
            return -1, -1
        #bounds include large silence
        if cand1 <= ss[0] and cand2 >= ss[1]:
            left = -1
            right = -1
            return left,right
    return left,right

# Creates the set of spans for the search space
def createSearchSpace(b,sil,e,num, rate):
    bounds = np.loadtxt(b, dtype=int)
    with open(sil, 'r') as sf:
        ls = sf.readlines()
    sils = []
    for l in ls:
        gggg = (l.strip()).split()
        sils.append((int(gggg[0]),int(gggg[1])))
    
    bounds = bounds/int(rate)
    starts = [0]
    for bb in bounds:
        if bb > starts[-1] + 5:
            starts.append(bb)
    if e not in starts:
        starts.append(e)
    
    bs = np.linspace(0, e, num+1, dtype=int)
    for bb in bs:
        if bb not in starts:
            bisect.insort_left(starts,bb)
    
    ans = []
    for i in starts:
        for j in starts:
            if j>i:
                l,r = checkSpace(sils,i,j)
                if l != -1:
                    ans.append((l,r))
    return list(set(ans))

# Probability at E-step
def getProb(k, frames, starts):
    global Prototype
    prob2 = []
    if (k == 0) or (len(Prototype[k-1])==0):
        for st in starts:
            prob2.append(-9.99)
        return prob2
    Z = 0.0
    ccs = []
    for st in starts:
        ccs.append(np.exp(-(score(Prototype[k-1], frames[st[0]:st[1]], True)**2)))
        Z += ccs[-1]
    if Z > 0:
        Z = np.log(Z)
    else:
        Z = np.inf
    for c in ccs:
        if not (Z == np.inf):
            prob2.append(np.log(c) - Z)
        else:
            prob2.append(-9.99)
    return prob2
    
# For parallelization
def getProb_star(a_b):
    return getProb(*a_b)

# Find best alignment
def align(ind, sent, frames, starts, lamda):
    global aligns
    global vocab
    global count
    global prob
    global Prototype
    
    # TO-DO: Make number of fixed classes a program parameter, not hard coded
    NumClasses = 2*len(mapkey)
    lensent = len(sent)
    lensig = len(frames)
    tempmu = int(np.floor((lensig-1)/lensent))
    
    likelihood = 0

    # Get the classes that map to the words of the sentence
    ks = []
    for word in sent:
        if word in mapkey:
            ks.append(mapkey[word][0])
            ks.append(mapkey[word][1])
    if len(ks)>0:
        pool = Pool(processes=8)
        prob2s = pool.map(getProb_star, itertools.izip(ks, itertools.repeat(frames), itertools.repeat(starts)))
        pool.close()
        pool.join() 


    charsentlen=0.00001
    for word in sent:
        charsentlen += len(word)
 
    for ei,word in enumerate(sent):
        selectk = 0
        indmins = (0,1)
        tmax = -np.inf
        # \mu for this word for fast-alin parametrization
        chartempmu = int(np.floor(lensig*len(word)/charsentlen))
        
        # If word doesn't have a class, send it to class 0 (only used for initialization)
        if word not in mapkey:
            wordlist = [0]
        else:
            wordlist = ks
        for ki,k in enumerate(wordlist):
            for sti,st in enumerate(starts):
                i = st[0]
                j = st[1]
                if (i,ei,lensent,lensig,chartempmu) not in DX:
                    DX[(i,ei,lensent,lensig,chartempmu)] = np.log(fastd.dx(i,ei,lensent,lensig,chartempmu,lamda))
                if (j,ei,lensent,lensig,chartempmu) not in DY:
                    DY[(j,ei,lensent,lensig,chartempmu)] = np.log(fastd.dy(j,ei,lensent,lensig,chartempmu,lamda))
                if word in mapkey:
                    sc = prob[word,k] + prob2s[ki][sti] + DX[(i,ei,lensent,lensig,chartempmu)] + DY[(j,ei,lensent,lensig,chartempmu)]
                else:
                    sc = DX[(i,ei,lensent,lensig,chartempmu)] + DY[(j,ei,lensent,lensig,chartempmu)]
                if sc > tmax:
                    selectk = k
                    indmins = (i,j)
                    tmax = sc
        aligns[ind].append((selectk, indmins))
        vocab[selectk].append((ind,indmins[0],indmins[1]))
        count[word] += 1
        count[word,selectk] += 1
        likelihood += tmax
    return likelihood

# Used in initialization
def init(inds, sents):
    global aligns
    global vocab
    global mapkey
    global count    
    count.clear()
    # Get unique words
    for i in inds:
        for word in sents[i]:
            if word not in mapkey:
                mapkey[word] = (2*len(mapkey) + 1,2*len(mapkey) + 2)
    NumClasses = 2*len(mapkey)
    # Init vocab
    vocab = []
    for i in xrange(NumClasses + 1):
        vocab.append([])

    for i in inds:
        for j,word in enumerate(sents[i]):
            if (aligns[i][j][0] == 0):
                start = aligns[i][j][1][0]
                end = aligns[i][j][1][1]
                if end > start:
                    if len(vocab[mapkey[word][0]]) <= len(vocab[mapkey[word][1]]):
                        vocab[mapkey[word][0]].append((i,start,end))
                        count[word, mapkey[word][0]] += 1
                    else:
                        vocab[mapkey[word][1]].append((i,start,end))
                        count[word, mapkey[word][1]] += 1
                #count[word, mapkey[word]] += 1
                count[word, 0] = 0
                count[word] += 1
    return

# For M-step
def getPrototypes(NumClasses):
    global Prototype
    
    del Prototype[:]
    AllProts = []
    
    # Parallel version
    pool = Pool(processes=8)
    Prots = pool.map(dba,xrange(1,NumClasses+1))
    pool.close()
    pool.join()
    for k in range(1,NumClasses+1):
        Prototype.append(np.array(Prots[k-1]))
    
    del Prots[:]
    del Prots
    return

# Utils
def readdict(f):
    return pickle.load(open(f,"rb"))

def savedict(d, f):
    pickle.dump(d, open(f,"wb"))
    return

def main(): 
    global aligns
    global prob
    global vocab
    global DX
    global DY
    global mapkey
    global count
    global searchSpace
    global MFCCptr

    parser = argparse.ArgumentParser(description='Hard EM with Curriculum for word-level alignment.')
    parser.add_argument('-m', required=True, type=int, help='Run EM up to translations this length.')
    parser.add_argument('-k', required=True, type=int, help='Lamda value for fastalign.')
    parser.add_argument('-a', required=True, type=str, help='Directory to save dicts.')
    parser.add_argument('-o', required=True, type=str, help='Directory to write output alignments.')
    parser.add_argument('-t', required=True, type=int, help='EM iterations for each length')
    parser.add_argument('-d', required=True, type=str, help='Translations file')
    parser.add_argument('-l', required=True, type=str, help='Labels file')
    parser.add_argument('-f', required=True, type=str, help='MFCC dir')
    parser.add_argument('-b', required=True, type=str, help='Bounds dir')
    parser.add_argument('-j', required=True, type=str, help='Silences dir')
    parser.add_argument('-r', required=True, type=int, help='The rate at which the silences were computed. (8kHz or 44.1kHz)')
    parser.add_argument('-p', required=True, type=float, help='Add -p smoothing')
    args = parser.parse_args()
    m = args.m
    t = args.t
    outf = args.o
    alf = args.a
    trd = args.d
    lbf = args.l
    mfcf = args.f
    boundsf = args.b
    silencesf = args.j
    lamda = args.k
    delta = args.p
    rate = args.r
    
    #Read labels
    lf = open(lbf, 'r')
    labels = lf.readlines()
    lf.close()
    for i,l in enumerate(labels):
        labels[i] = l.strip()

    # Read translations and limit to len(labels)
    with codecs.open(trd, 'r', 'utf-8') as inf:
        trans = inf.readlines()
    # Modification for running on long's data
    '''
    with open("/afs/crc.nd.edu/group/nlp/08/aanastas/Spanish/files.txt", 'r') as lf:
        AllLabels = lf.readlines()
    for i,l in enumerate(AllLabels):
        AllLabels[i] = l.strip()
    trans2 = []
    for l in labels:
        trans2.append(trans[AllLabels.index(l)])
    trans = trans2
    assert len(trans)==len(labels)
    '''
    trans = trans[:len(labels)]

    # Read features
    mfcc = []
    for l in labels:
        mfcc.append(np.loadtxt(mfcf + l + ".mfc"))
    

    MFCCptr = mfcc

    # Deal with max sentence length
    maxm = 0
    for tr in trans:
        sen = (tr.strip()).split()
        if maxm < len(sen):
            maxm = len(sen)	
    if m > maxm:
        m = maxm
    
    #Modify translation sentences
    sents = []
    for tr in trans:
	    s = (tr.strip()).split()
	    sents.append(s)
    for i,s in enumerate(sents):
        for w in s:
            if w == "":
                s.remove(w)
    del trans[:]
    del trans

    #Create Search Space given the boundaries and the silences
    for rr,l in enumerate(labels):
        ts = createSearchSpace(boundsf+l+".bounds", silencesf+l+".txt", len(mfcc[rr]),len(sents[rr]),rate/100.0)
        searchSpace.append(ts)
    
    NumClasses = 0
    
    #Select data with length up to m:
    inds = [j for j,s in enumerate(sents) if len(s) <= maxm]
     
    # EM iterations
    for j in range(t):
        print "******************************************"
        print "EM iteration ", j

        # Create dir to write the found alignments
        tempout = outf + "out-"+str(j)
        try:
            os.mkdir(tempout)
        except:
            pass
        
        # Init
        if j == 0:
            for i in xrange(len(sents)):
                aligns.append([])
            for d in inds:
                # Hard E-step
                sent_likelihood = align(d, sents[d], mfcc[d], searchSpace[d], lamda)
                # Write output
                of = codecs.open(tempout + "/" + labels[d] + ".words", 'w', 'utf-8')
                for ei,word in enumerate(sents[d]):
                    start = int(aligns[d][ei][1][0])
                    end = int(aligns[d][ei][1][1])
                    cl = int(aligns[d][ei][0])
                    of.write(word + " " + str(start) + " " + str(end) + " " + str(cl) + "\n")
                of.close()
            # Init some variables
            init(inds,sents)
            NumClasses = 2*len(mapkey)
        # E-step
        else:
            total_likelihood = 0
            count.clear()
        
            # Clean up vocab
            for v,_ in enumerate(vocab):
                vocab[v] = []
		    
            # Clean up aligns
            for d in inds:
                aligns[d] = []    
            for d in inds:
                sent_likelihood  = align(d, sents[d], mfcc[d], searchSpace[d],lamda)
                if sent_likelihood > -np.inf:
                    total_likelihood += sent_likelihood
                # Write found alignments
                of = codecs.open(tempout + "/" + labels[d] + ".words", 'w', 'utf-8')
                for ei,word in enumerate(sents[d]):
                    start = int(aligns[d][ei][1][0])
                    end = int(aligns[d][ei][1][1])
                    cl = int(aligns[d][ei][0])
                    of.write(word + " " + str(start) + " " + str(end) + " " + str(cl) + "\n")
                of.close()
            print "Iteration total likelihood: ", total_likelihood
            
        # Use add-one smoothing to get probabilities for translation component
        for word in mapkey:
            den = np.log(float(count[word] + delta*2))
            for k in [mapkey[word][0],mapkey[word][1]]:
                prob[word,k] = np.log(float(count[word,k] + delta))-den
        
        # M-step
        # Get prototypes
        getPrototypes(NumClasses)
        
        # Save alignment dictionary for future reference
        savedict(aligns, alf+"aligns."+str(j)+".dict")
        savedict(prob, alf+"prob."+str(j)+".dict")
        savedict(DX, alf+"DX."+str(j)+".dict")
        savedict(DY, alf+"DY."+str(j)+".dict")
        print "Saved all the necessary dictionaries"
        
    
    # Write the final alignments
    tempout = outf + "out-final"
    try:
        os.mkdir(tempout)
    except:
        pass
	
    for d in inds:
		sent = sents[d]
		of = codecs.open(tempout + "/" + labels[d] + ".words", 'w', 'utf-8')
		for ei,word in enumerate(sent):
			start = int(aligns[d][ei][1][0])
			end = int(aligns[d][ei][1][1])
			cl = int(aligns[d][ei][0])
			of.write(word + " " + str(start) + " " + str(end) + " " + str(cl) + "\n")
		of.close()

if __name__ == "__main__":
    main()
