import numpy as np
import codecs
import argparse
import os
import string
from collections import defaultdict
import re

regex = re.compile('[%s]' % re.escape(string.punctuation))

def is_directory(arg):
	if not os.path.isdir(arg):
		return False
	else:
		return True

def is_file(arg):
	if not os.path.isfile(arg):
		return False
	else:
		return True

def readGoldData(fn):
	try:
		f = codecs.open(fn, 'r', 'utf-8')
	except IOError:
		print "Could not read the gold file provided:", fn
		return 0
	data = f.readlines()
	f.close()
	words = []
	start = []
	end = []
	for d in data:
		tmp = (d.strip()).split()

		words.append(regex.sub('', tmp[0].lower()))
		start.append(int(float(tmp[1])*100))
		end.append(int(float(tmp[2])*100))
	return words, start, end


def readData(fn):
	try:
		f = codecs.open(fn, 'r', 'utf-8')
	except IOError:
		print "Could not read Outfile provided:", fn
		return 0
	data = f.readlines()
	f.close()
	words = []
	start = []
	end = []
	for d in data:
		tmp = (d.strip()).split()
		words.append(regex.sub('', tmp[0].lower()))
		start.append(int(tmp[1]))
		end.append(int(tmp[2]))
	return words, start, end

def eval_count(outfile, goldfile,m):
    gwords, gstart, gend = readGoldData(goldfile)
    owords, ostart, oend = readData(outfile)
    if len(gstart) > m:
        return -1,-1,-1
    found = 0
    outs = defaultdict(lambda: [])
    for i,_ in enumerate(ostart):
        for j in range(ostart[i], oend[i]):
            outs[j].append(owords[i])
        found += oend[i] - ostart[i]
    corr = 0
    cor_ret = 0
    for i,_ in enumerate(gstart):
        for j in range(gstart[i], gend[i]):
            if gwords[i] in outs[j]:
                cor_ret += 1
        corr += gend[i] - gstart[i]
    
    return float(cor_ret), float(corr), float(found)
	




def main():
    parser = argparse.ArgumentParser(description='Given a gold directory/file and an output alignment directory/file, calculate precision, recall, F1-score.')
    
    parser.add_argument('-i', nargs=1, required=True, type=str, help='The directory/filename of the produced alignments.')
    parser.add_argument('-g', nargs=1, required=True, type=str, help='The directory/filename of the gold alignments.')
    parser.add_argument('-l', nargs=1, required=True, type=str, help='The labels file.')
    parser.add_argument('-m', nargs=1, required=True, type=int, help='The length we are going up to.')
    parser.add_argument('-o', nargs=1, help='The file to write the output.')
    
    args = parser.parse_args()
    infn = args.i[0]
    goldfn = args.g[0]
    pr = False
    if not(args.o is None):
        pr = True
        outfn = args.o[0]
    lab = args.l[0]
    labels = open(lab, 'r').readlines()
    m = args.m[0]
    if (is_directory(infn) and is_directory(goldfn)):
        if pr:
            out = open(outfn, 'w')
            out.write("File\tPrecision\tRecall\tF1\n")
            out.write("----------------------------\n")
        for tttt in range(1):
            Correct = 0.0
            Retrieved = 0.0
            CorrAndRet = 0.0
            #if pr and (outfn.rpartition('/')[2] in files): 
            #	files.remove(outfn.rpartition('/')[2])
            for l in labels:
                try:
                    #fname = (f.rpartition('.'))[0]
                    #fnn = os.path.join(subdir, f)
                    #cor_ret, cor, ret = eval_count(fnn,goldfn+fname+'.words')
                    iiif = infn+l.strip()+".words"
                    #if len(l.strip()) == 1:
                    #    iiif = infn+"000.00"+l.strip()+".words"
                    #if len(l.strip()) == 2:
                    #    iiif = infn+"000.0"+l.strip()+".words"
                    #if len(l.strip()) == 3:
                    #    iiif = infn+"000."+l.strip()+".words"
                    cor_ret, cor, ret = eval_count(iiif, goldfn+l.strip()+'.en',m)
                    if cor_ret >= 0 and cor >= 0 and ret >= 0:
                        Correct +=cor
                        Retrieved += ret
                        CorrAndRet += cor_ret
                        try:
                            fprec = (cor_ret/ret)
                        except ZeroDivisionError:
                            fprec = 0.0
                        try:
                            frec = (cor_ret/cor)
                        except ZeroDivisionError:
                            frec = 0.0
                        try:
                            ff1 = 2 * fprec * frec / (fprec + frec)
                        except ZeroDivisionError:
                            ff1 = 0.0
                        if pr:
                            out.write(l.strip() + "\t" + str(fprec)+"\t"+str(frec)+"\t"+str(ff1)+"\n")
                        else:
                            print l.strip(), fprec, frec, ff1
                except:
                    None
        Precision = CorrAndRet / Retrieved
        Recall = CorrAndRet / Correct
        F1 = 2 * Precision * Recall / (Precision + Recall)
        if pr:
            out.write("-----------------------------\n")
            out.write("Total Precision:" + str(Precision) + "\n")
            out.write("Total Recall:" + str(Recall) + "\n")
            out.write("Total F1-score:" + str(F1) + "\n")
            out.close()
        else:
            print "Total Precision:", Precision
            print "Total Recall:", Recall
            print "Total F1-score:", F1
    else:
        print "Error! Please make sure that you are providing correct paths."

			



if __name__ == "__main__":
    main()
