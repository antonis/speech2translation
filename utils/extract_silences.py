import argparse
import os 
from multiprocessing import Pool


def run_silence((inputf,outputf)):
    print 'Extracting candidate silences for file  : ', inputf
    silencepath = '/afs/crc.nd.edu/user/a/aanastas/preprocessing/silence.py'
    cmd = 'python '+ silencepath + ' -f '+ inputf + ' -c 1 -t 0.1 -o ' + outputf 
    os.system(cmd)

def read_corpus_wav(fname,fout, ext, threads_no):
    print 'Read corpus from folder ', fname, ' only read files that end with ', ext
    all_paths = []
    for file_name in os.listdir(fname): 
        if (file_name.find(ext) == -1): continue
        new_file_name = file_name.replace(ext,'')
        full_path_in = fname + '/' + file_name
        full_path_out = fout + '/' + new_file_name +'.txt'
        all_paths.append((full_path_in,full_path_out))
    pool = Pool(threads_no)
    pool.map(run_silence, all_paths)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Calculate candidate silences with silence.py from ')
    
    parser.add_argument('-i','--input', help='Input folder where the audio is stored', required=True)
    parser.add_argument('-o','--output', help='Output cadidate silences folder  ', required=True)
    parser.add_argument('-t','--thread', help='Number of threads', required=False)
    args = parser.parse_args()
    
    no_threads = 1 
    if args.thread != None:
        no_threads = int(args.thread)
    read_corpus_wav(args.input, args.output, '.wav', no_threads)
    
