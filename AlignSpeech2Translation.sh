#!/bin/bash

dataset=data/Griko-Italian
# Output folder
outf=$dataset/results
# Store intermediate results here
alf=$outf/aligns/
mkdir $outf
mkdir $alf
mkdir $outf/evals/

# Store outputs here
res=$outf/outputs/
mkdir $res

# Datapaths
trans=$dataset/translations.txt
labels=$dataset/files.txt
raw=$dataset/raw/
mfcc=$dataset/plp/
bounds=$dataset/bounds/
silences=$dataset/silences/
# Signal rate for the dataset
rate=44100


# Set parameters
# Lamda for fast_align
lam=0.5
# Number of EM iterations
t=5
# Run for sentences up to length:
m=20
# Add-p smoothing
p=0.01

# Align
python code/align-speech-to-translation.py -m $m -k $lam -a $alf -o $res -t $t -d $trans -l $labels -f $mfcc -b $bounds -j $silences -p $p -r $rate > $outf/log.txt

# Evaluate
workd=/afs/crc.nd.edu/group/nlp/08/aanastas/


gold=$dataset/wav2it/

# for number of iterations
for m in 0 1 2 3 4
do
    python utils/eval-length.py -i $res/out-$m/ -g $gold -l $labels -m $m -o $outf/evals/eval.$m.txt
done






